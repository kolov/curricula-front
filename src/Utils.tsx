import React from "react"

export const randomString = (n: number) => {
    var s = ''
    for (let i = 0; i < n; i++) {
        s = s + String.fromCharCode(Math.random() * 26 + 'a'.charCodeAt(0))
    }
    return s
}

export const idFromPath = (path: string) => {

    return path.replace(/\[\]\//g, '_')
}

export const ApplicationName = 'Curricu.li'
// export const CognitoUrl= 'https://curriculi.auth.us-east-1.amazoncognito.com'
// export const CognitoClientId= '67dcvo7lle0f9rm7tr5miund1'
export const CognitoUrl= 'https://curriculi.auth.us-east-1.amazoncognito.com'
export const CognitoClientId= '2a1eq1s26s0qeq78k9dc5busdg'
 

export const ApplicationNameSpan = () => {
    return <span className="app-name">{ApplicationName}&nbsp;</span>
}