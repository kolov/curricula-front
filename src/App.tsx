import React, { useState, useEffect, createContext } from 'react';
import './App.css';
import { MainPage } from './page/MainPage'
import { EditDocument } from './page/EditDocument'
import { CurrentUser } from './component/CurrentUser'
import { Navbar, Nav, NavbarBrand, NavLink, NavItem, NavbarToggler, Container } from 'reactstrap';
import { AcceptCookies } from './component/AcceptCookies';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  RouteComponentProps
} from "react-router-dom";
import { NotFound, SomethingWentWrong } from './component/ErrorPages';
import { AboutPage } from './page/AboutPage';
import { ApplicationName } from './Utils';
import { FormattedDocument } from './component/FormattedDocument';
import { PrivacyPage } from './page/PrivacyPage';
import { DocumentsPage } from './page/DocumentsPage';
import { TemplatesPage } from './page/TemplatesPage';
import { CognitoLoginCallback, CognitoLogoutCallback } from './page/CognitoLoginCallback';

export interface AppUser {
  id: String,
  authenticated: Boolean,
  acceptedCookies: Boolean,
  name?: string,
  picture?: string
}

export const UserContext = createContext<AppUser | undefined>(undefined)

const App: React.FC = () => {


  return (

    <div >
      <Router>
        <Switch>
          <Route path="/preview/:id" component={DocumentPreview} />
          <Route path="/" component={Xxx} />
        </Switch>
      </Router>
    </div>

  );
}

type TParams = { id: string };

export const DocumentPreview = ({ match }: RouteComponentProps<TParams>) => {
  return <div style={{ width: '100%', padding: '20%', background: 'white' }}><FormattedDocument uuid={match.params.id} /></div>
}
export const Xxx = () => {
 
  const [user, setUser] = useState<AppUser | undefined>(undefined);

  const updateUser = (url: RequestInfo) => fetch(url, {
    credentials: 'include'
  })
    .then(
      function (response) {
        if (response.status !== 200) {
          return;
        }

        // Examine the text in the response
        response.json().then(data => {
          const updatedUser = {
            id: data['uuid'].toString(),
            authenticated: data['authenticated'],
            acceptedCookies: data['acceptedCookies'],
            name: data['name'],
            picture: data['picture']
          }
          console.log(`Updated user to ${updatedUser}`)
          setUser(updatedUser)
        });
      }
    )
    .catch(err => console.log('Fetch Error :-S', err))

  useEffect(() => {
    updateUser('/api/user/info')
  }, []);

  return <div>
    <div className="bg-gray-800 pb-32">
      <nav className="bg-gray-800">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="border-b border-gray-700">
            <div className="flex items-center justify-between h-16 px-4 sm:px-0">
              <div className="flex items-center">
                <div className="flex-shrink-0">
                <a href="/" className="px-3 py-2 rounded-md text-bg font- text-white bg-gray-900 focus:outline-none focus:text-white focus:bg-gray-700">Curricu.li</a>
                </div>
                <div className="hidden md:block">
                  <div className="ml-10 flex items-baseline space-x-4">
                    <a href="/documents" className="px-3 py-2 rounded-md text-sm font-medium text-gray-300 focus:outline-none focus:text-white focus:bg-gray-700">Documents</a>
                    <a href="/templates" className="px-3 py-2 rounded-md text-sm font-medium text-gray-300 focus:outline-none focus:text-white focus:bg-gray-700">Templates</a>

                    <a href="/about"     className="px-3 py-2 rounded-md text-sm font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">About</a>

                    <a href="/privacy" className="px-3 py-2 rounded-md text-sm font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Privacy</a>
                  </div>
                </div>
              </div>
              <div className="hidden md:block">
                <div className="ml-4 flex items-center md:ml-6">
                  <button className="p-1 border-2 border-transparent text-gray-400 rounded-full hover:text-white focus:outline-none focus:text-white focus:bg-gray-700" aria-label="Notifications">
                    <svg className="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                    </svg>
                  </button>

                  {/* <!-- Profile dropdown --> */}
                  <CurrentUser user={user} updateUser={updateUser}/>
                </div>
              </div>
              <div className="-mr-2 flex md:hidden">
                {/* <!-- Mobile menu button --> */}
                <button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:bg-gray-700 focus:text-white">
                  {/* <!-- Menu open: "hidden", Menu closed: "block" --> */}
                  <svg className="block h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
                  </svg>
                  {/* <!-- Menu open: "block", Menu closed: "hidden" --> */}
                  <svg className="hidden h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                  </svg>
                </button>
              </div>
            </div>
          </div>
        </div>

        {/* <!--
        Mobile menu, toggle classes based on menu state.

        Open: "block", closed: "hidden"
      --> */}
        <div className="hidden border-b border-gray-700 md:hidden">
          <div className="px-2 py-3 space-y-1 sm:px-3">
            <a href="#" className="block px-3 py-2 rounded-md text-base font-medium text-white bg-gray-900 focus:outline-none focus:text-white focus:bg-gray-700">Dashboard</a>

            <a href="#" className="block px-3 py-2 rounded-md text-base font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Team</a>

            <a href="#" className="block px-3 py-2 rounded-md text-base font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Projects</a>

            <a href="#" className="block px-3 py-2 rounded-md text-base font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Calendar</a>

            <a href="#" className="block px-3 py-2 rounded-md text-base font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Reports</a>
          </div>
          <div className="pt-4 pb-3 border-t border-gray-700">
            <div className="flex items-center px-5 space-x-3">
              <div className="flex-shrink-0">
                <img className="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
              </div>
              <div className="space-y-1">
                <div className="text-base font-medium leading-none text-white">Tom Cook</div>
                <div className="text-sm font-medium leading-none text-gray-400">tom@example.com</div>
              </div>
            </div>
            <div className="mt-3 px-2 space-y-1" role="menu" aria-orientation="vertical" aria-labelledby="user-menu">
              <a href="#" className="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700" role="menuitem">Your Profile</a>

              <a href="#" className="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700" role="menuitem">Settings</a>

              <a href="#" className="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700" role="menuitem">Sign out</a>
            </div>
          </div>
        </div>
      </nav>
      <header className="py-10">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl leading-9 font-bold text-white">
          <Router>
              <Switch>
                <Route path="/about" children={<div>About</div>} />
                <Route path="/privacy" children={<div>Privacy</div>} />
                <Route path="/documents" children={<div>Documents</div>} />
                <Route path="/templates" children={<div>Templates</div>} />
                <Route path="/edit-document/:id" children={<div>Edit</div>} />
                <Route path="/cognito/login" children={<div></div>} />
                <Route path="/not-found" children={<div>Error</div>} />
                <Route path="/error" children={<div>Error</div>} />
                <Route path="/" children={<div>Home</div>} />
              </Switch>
            </Router>
        </h1>
        </div>
      </header>
    </div>

    <main className="-mt-32">
      <div className="max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:px-8">
        {/* <!-- Replace with your content --> */}
        <div className="bg-white rounded-lg shadow px-5 py-6 sm:px-6">
          {/* <div className="border-4 border-dashed border-gray-200 rounded-lg h-96"> */}
            <Router>
              <Switch>
                <Route path="/about" component={AboutPage} />
                <Route path="/privacy" component={PrivacyPage} />
                <Route path="/documents" component={DocumentsPage} />
                <Route path="/templates" component={TemplatesPage} />
                <Route path="/edit-document/:id" component={EditDocument} />
                <Route path="/cognito/login" children={ CognitoLoginCallback } />
                <Route path="/cognito/logout" children={ CognitoLogoutCallback } />
                <Route path="/not-found" component={NotFound} />
                <Route path="/error" component={SomethingWentWrong} />
                <Route path="/" component={DocumentsPage} />
              </Switch>
            </Router>
          {/* </div> */}
        </div>

        {/* <!-- /End replace --> */}
      </div>
    </main>
  </div>
}

export default App;
