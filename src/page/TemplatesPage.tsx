import React from 'react';
import { TemplatesList } from '../component/TemplatesList'
 


export const TemplatesPage = () => {

    return (
        <div>
            <p  >
                Choose a template below and start building yout CV. Templates describe the structure of the data is in the CV. The same date can be
                presented in different formats. You can clone an existing template and tweak it,
                or replace it completely.
                A CV document created from a template can be rendered in different formats.
                &nbsp;
                    <a href="/about">Learn more</a>

            </p>
            <div> <TemplatesList /></div>
        </div>
    );
}

