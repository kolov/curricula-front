import React, { useState, useEffect } from 'react';
import { Markup } from 'interweave';

import './PreviewDocument.css'
import { CodeEditor } from '../component/CodeEditor';
import { useHistory } from 'react-router-dom';
import { Button } from 'reactstrap';
import { DocumentRefreshContext } from '../component/document/GroupInput';

class DocumentPreview {
    uuid: string;
    body: any;
    constructor(json: any) {
        this.uuid = json['uuid']
        this.body = json['body']
    }
}

class DocumentOption {
    name: string;
    uuid: string;
    content: string;
    default: boolean;
    selected: boolean;

    constructor(json: any) {
        this.name = json['name']
        this.uuid = json['uuid']
        this.content = json['content']
        this.default = json['default']
        this.selected = json['selected']
    }

}

export class DocumentOptions {
    styles: Array<DocumentOption>;
    labels: Array<DocumentOption>;

    constructor(json: any) {
        this.styles = json['styles']
        this.labels = json['labels']
    }

}

export type DocumentPreviewComponentProps = {
    id: string
}

export const DocumentPreviewComponent = ({ id }: DocumentPreviewComponentProps) => {
    var wsUrl = 'wss://' + window.location.hostname + '/api/ws'

    const [documentPreview, setDocumentPreview] = useState<DocumentPreview | null>(null);
    const [documentStyle, setDocumentStyle] = useState<string>('');
    const [documentLabels, setDocumentLabels] = useState<string>('');
    const [editedStyle, setEditedStyle] = useState<string>('');
    const [editedLabels, setEditedLabels] = useState<string>('');
    const [documentOptions, setDocumentOptions] = useState<DocumentOptions>(new DocumentOptions({ "styles": [], "labels": [] }));
    const [showStyleEditor, setShowStyleEditor] = useState<boolean>(false);
    const [showLabelsEditor, setShowLabelsEditor] = useState<boolean>(false);
    const [ws, setWs] = useState<WebSocket | undefined>(undefined);

    useEffect(() => {
        getDocumentOptions()
    }, []);

    useEffect(() => {
        const ws = new WebSocket(wsUrl)
        initWs(ws)
        setWs(ws)
    }, []);


    const refreshDocument: () => void = React.useContext(DocumentRefreshContext)!

    const getDocumentOptions = () => {
        fetch(`/api/preview/options/${id}`)
            .then(response => response.json())
            .then(x => new DocumentOptions(x))
            .then(options => {
                setDocumentOptions(options);
                options.styles.filter(s => s.selected).forEach(s => setDocumentStyle(s.content))
                options.labels.filter(s => s.selected).forEach(s => setDocumentLabels(s.content))
            })
            .catch(err => console.log(err))
    }



    const updateDocumentRendering = (styleId: string | undefined, labelsId: string | undefined) => {
        fetch(`/api/document/${id}`, {
            method: 'POST', body: JSON.stringify({
                'style': styleId ? styleId : null,
                'labels': labelsId ? labelsId : null
            })
        })
            .then(x => {
                console.log('Calling refresh document')
                refreshDocument!()
            })
            .catch(err => console.log(err))
    }


    const initWs = (ws: WebSocket) => {
        console.log('initWs')
        ws.onopen = () => {
            // on connecting, do nothing but log it to the console
            console.log('connected')
            ws.send(`{"command": "subscribe", "documentId": "${id}"}`)
        }

        ws.onmessage = evt => {
            // on receiving a message, add it to the list of messages    
            console.log('got ws update')
            const updated = new DocumentPreview(JSON.parse(evt.data))
            if (updated['body']) {
                setDocumentPreview(updated)
            }
        }

        ws.onclose = () => {
            console.log('disconnected')
            // automatically try to reconnect on connection loss
            initWs(ws);
        }


    }

    const changeStyle = (uuid: string) => {
        const found = documentOptions.styles.find(s => s.uuid == uuid)
        if (found) {
            setDocumentStyle(found.content)
            updateDocumentRendering(uuid, undefined)
            setDocumentOptions(new DocumentOptions({
                "styles": documentOptions.styles.map(s =>
                    new DocumentOption({ 'uuid': s.uuid, 'name': s.name, 'content': s.content, 'default': s.default, 'selected': s.uuid == uuid })),
                "labels": documentOptions.labels
            }))
        }
    }

    const changeLabels = (uuid: string) => {
        const found = documentOptions.labels.find(s => s.uuid == uuid)
        if (found) {
            updateDocumentRendering(undefined, uuid)
            setDocumentLabels(found.content)
            setDocumentOptions(new DocumentOptions({
                "styles": documentOptions.styles.map(s =>
                    new DocumentOption({ 'uuid': s.uuid, 'name': s.name, 'content': s.content, 'default': s.default, 'selected': s.uuid == uuid })),
                "labels": documentOptions.labels.map(l =>
                    new DocumentOption({ 'uuid': l.uuid, 'name': l.name, 'content': l.content, 'default': l.default, 'selected': l.uuid == uuid }))
            }))
        }
    }

    const toggleEditStyle = () => {
        setEditedStyle(documentStyle)
        setShowStyleEditor(!showStyleEditor)
        setShowLabelsEditor(showLabelsEditor && showStyleEditor)
    }

    const toggleEditLabels = () => {
        setEditedLabels(documentLabels)
        setShowLabelsEditor(!showLabelsEditor)
        setShowStyleEditor(showStyleEditor && showLabelsEditor)
    }

    const applyStyle = () => {
        setDocumentStyle(editedStyle)
    }

    const applyLabels = () => {
        alert('Not implemented')
    }

    const styleSelector = () => {
        const selectedStyle = documentOptions.styles.find(s => s.selected)
        var selectedValue
        if (selectedStyle)
            selectedValue = selectedStyle.uuid
        else
            selectedValue = ''

        return (<select
            disabled={showStyleEditor}
            className="custom-select"
            value={selectedValue}
            onChange={e => changeStyle(e.target.value)}>
            {
                documentOptions.styles.map(s => (<option key={s.uuid} value={s.uuid}>{s.name}</option>))
            }
        </select>)

    }

    const labelsSelector = () => {
        const selectedLabels = documentOptions.labels.find(s => s.selected)
        var selectedValue
        if (selectedLabels)
            selectedValue = selectedLabels.uuid
        else
            selectedValue = ''

        return (<select disabled={showLabelsEditor}
            className="custom-select"
            value={selectedValue}
            onChange={e => changeLabels(e.target.value)}>
            {
                documentOptions.labels.map(s => (<option key={s.uuid} value={s.uuid}>{s.name}</option>))
            }
        </select>)

    }

    const renderCsEditor = () => {
        return (<div><div id="curri-css-editor" >
            <CodeEditor name='css-editor' content={documentStyle}
                lang='css'
                changeHandler={(s: string) => setEditedStyle(s)} />
        </div>
            <div>
                <Button outline={true} size="sm" onClick={applyStyle}>Apply</Button>
                <Button outline={true} size="sm" >Save As...</Button>
            </div>
        </div>)
    }

    const renderLabelsEditor = () => {
        return (<div><div id="curri-labels-editor" >
            <CodeEditor name='labels-editor' content={editedLabels}
                lang='properties'
                changeHandler={(s: string) => setEditedLabels(s)} />
        </div>
            <div>
                <Button outline={true} size="sm" onClick={applyLabels}>Apply</Button>
                <Button outline={true} size="sm" >Save As...</Button>
            </div>
        </div>)
    }

    if (documentPreview) {

        return (

            <div id='curri-document-preview'>
                {/* Style Slection */}
                <div className="input-group mb-2">
                    <div className="input-group-prepend">
                        <label className="input-group-text">Style</label>
                    </div>
                    {styleSelector()}
                    <button type="button" className="btn btn-light" onClick={toggleEditStyle}>{showStyleEditor ? 'Hide' : 'Show'}</button>
                </div>

                {/* Labels Slection */}
                <div className="input-group mb-2">
                    <div className="input-group-prepend">
                        <label className="input-group-text">Labels</label>
                    </div>
                    {labelsSelector()}
                    <button type="button" className="btn btn-light" onClick={toggleEditLabels}>{showLabelsEditor ? 'Hide' : 'Show'}</button>
                </div>

                <div>
                    <div id="curri-css-edit-block" >
                        <style type="text/css">
                            {documentStyle}
                        </style>
                        {showStyleEditor ?
                            renderCsEditor()
                            : <div></div>}
                        {showLabelsEditor ?
                            renderLabelsEditor()
                            : <div></div>}

                    </div>

                </div>

                <div id='curri-preview-document'>
                    <Markup content={documentPreview.body}></Markup>
                </div>
            </div>)
    } else {
        return (<div className={`cu-around-preview`}> Updating... </div>)
    }

}
