import React from 'react';
import { TemplatesList } from '../component/TemplatesList'
import { DocumentsList } from '../component/DocumentsList'
import { Row, Col, Table } from 'reactstrap';
import {
    RouteComponentProps
} from "react-router-dom";
import { ApplicationName } from '../Utils';


export const DocumentsPage = () => {

    return (
        <div>
            <DocumentsList /> 
        </div>
    );
}

