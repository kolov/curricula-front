import React from 'react';
import { Row, Col } from 'reactstrap';
import { ApplicationName, ApplicationNameSpan } from '../Utils';



export const AboutPage = () => {

    return ( <div>
                <h1>How it works</h1>
            
            
                <p><ApplicationNameSpan/>helps you create,
                    maintain and share a set of CVs for you or your company.</p>
                <p>
                    The building block of a CV ar the template, the labels, and the styles. {ApplicationName}&nbsp;
                    provides a few sets which you can use as a stting point and tweak them to fit your
                    peference.
                </p>
                <h4>Template</h4>
                <p>
                    The template describes the structure of the data in the CV. For example:
                    The Head section consits of a name, an email and optinal phone number.
                    The Projects section is a list of 1 or more projects. Each project has start date,
                    list of techniques etc. Templates are in JSON format. {ApplicationName}&nbsp; allows
                    you to maintan many templates.

                    The template describes the format of each field - text, multiline text, date etc. Once a tempalte is defined,
                    {ApplicationName}&nbsp; can rendered an inpt form to start filling in the data.

                </p>

                <h4>Labels</h4>
                <p>
                    The Labels provide translations for the section names in the tamplate. For example, you may want to
                    render the <code>work</code> section as <i>Work</i>, <i>Work Experience</i>, or maybe <i>Travail</i>.

                    Alo, they define hints for the input fields, like this one:


                </p>

                <h4>Styles</h4>
                <p>
                    The style determines how the data is shown - fonts, spacing, placement etc. You can preview the documents on the site,
                    download it as a PDF, or share a link to it.
                </p>

            
                <h3>Privacy</h3>
             
                See the <a href='/privacy'>Privacy Policy</a>
                </div>

    );
}

