import React from 'react';
import { TemplatesList } from '../component/TemplatesList'
import { DocumentsList } from '../component/DocumentsList'
import { Row, Col, Table } from 'reactstrap';
import {
    RouteComponentProps
} from "react-router-dom";
import { ApplicationName } from '../Utils';

type TParams = { id: string };

export const MainPage = ({ location, match }: RouteComponentProps<TParams>) => {

    return (
        <table>
            <thead>
                <tr>
                    <th scope="col">Templates</th>
                    <th scope="col">Documents</th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td style={{width:'50%'}}>
                        Choose a template below and start building yout CV. Templates describe the structure of the data is in the CV. The same date can be
                        presented in different formats. You can clone an existing template and tweak it,
                        or replace it completely.
                        A CV document created from a template can be rendered in different formats.
                        &nbsp;
                    <a href="/about">Learn more</a>

                    </td>
                    <td>sdsd</td>
                </tr>
                <tr>
                    <td> <TemplatesList /></td>
                    <td> <DocumentsList /></td>
                </tr>

            </tbody>


        </table >
    );
}

