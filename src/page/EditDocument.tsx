import React, { useState, useEffect } from 'react';
import { RouteComponentProps } from "react-router-dom";
import { Document } from '../model/DocumentModel'
import './EditDocument.css'
import { Row, Col } from 'reactstrap';
import { DocumentPreviewComponent } from './PreviewDocument';
import { DocumentIdContext, DocumentRefreshContext } from '../component/document/GroupInput';

type TParams = { id: string };


export const EditDocument = ({ match }: RouteComponentProps<TParams>) => {

  const [rendered, setRendered] = useState<JSX.Element | undefined>(undefined);
  const [document, setDocument] = useState<Document | undefined>(undefined);

  const fetchDocument = () => {
    console.log('Fetch doc called')
    fetch(`/api/document/${match.params.id}`)
      .then(response => response.json())
      .then(json => new Document(json))
      .then(doc => {
        setDocument(doc)
        setRendered(doc.renderForEdit())
      })
      .catch(err => console.log(err))
  }

  useEffect(() =>
    fetchDocument()
    , [match.params.id]);

  if (document) {
    return (


      <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
        <h3 className="text-lg leading-6 font-medium text-gray-900">
          Editing {document?.name}
        </h3>
        <Row>
          <Col>
            <DocumentIdContext.Provider value={match.params.id}>
              <DocumentRefreshContext.Provider value={fetchDocument}>
                <div>{rendered}</div>
              </DocumentRefreshContext.Provider>
            </DocumentIdContext.Provider>
          </Col>
          <Col>
            <DocumentRefreshContext.Provider value={fetchDocument}>
              <DocumentPreviewComponent id={match.params.id} />
            </DocumentRefreshContext.Provider>
          </Col>

        </Row>
      </div>)

  }
  else { return (<div>The document could not be found</div>) }

}