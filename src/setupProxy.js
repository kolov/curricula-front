const proxy = require('http-proxy-middleware');

module.exports = function (app) {
  const backendHost = process.env.BACKEND_HOST || 'localhost';
  const backendPort = process.env.BACKEND_PORT || 5566;

  const proxyTarget = 'http://' + backendHost + ':' + backendPort;
  console.log("proxyTarget=" + proxyTarget)
  app.use(
    '/api',
    proxy({
      target: proxyTarget,
      changeOrigin: false,
      pathRewrite: {
        '^/api/': '/' // remove base path
      }
    })
  );
};