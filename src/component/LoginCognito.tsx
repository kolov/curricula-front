
import React from 'react';
import { Button } from 'reactstrap';
import { CognitoClientId, CognitoUrl } from '../Utils';


const LoginCognito = () => {
  return <Button outline={true} color="primary" href={
    `${CognitoUrl}/oauth2/authorize?response_type=token&client_id=${CognitoClientId}&redirect_uri=https://${window.location.hostname}/cognito/login&state=STATE&scope=openid+profile+email`
  } >
    Login
  </Button>
}



export default LoginCognito;
