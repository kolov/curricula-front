import React, { useEffect, useState } from 'react';
import './AcceptCookies.css';

import { Markup } from 'interweave';

type Sample = {
  name: string,
  uuid: string,
  body: any,
  style: string
}

type FormattedDocumentProps = {
  uuid: string
}



export const FormattedDocument = ({ uuid }: FormattedDocumentProps) => {

  const [samplePreview, setSamplePreview] = useState<Sample | undefined>(undefined);


  const getDocumentFormatted = (id: string) => {
    fetch(`/api/document/${id}/formatted`)
      .then(response => response.json())
      .then(x => x as Sample)
      .then(x => {
        setSamplePreview(x);
      })
      .catch(err => console.log(err))
  }


  useEffect(() => {
    getDocumentFormatted(uuid)
  }, []);


  return (<div className="cu-body"><style type="text/css">
    {samplePreview?.style}
  </style>
    <Markup content={samplePreview?.body}></Markup>
  </div>)

}

