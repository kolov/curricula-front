import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router'
import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row, Tooltip } from 'reactstrap';
import { randomString } from '../Utils';
import { CodeEditor } from './CodeEditor';
import { FormattedDocument } from './FormattedDocument';
import { DocumentOptions } from '../page/PreviewDocument';
import * as icon from './Heroicons';
import { ItemMenu, MenuItem } from './TemplateItemMenu1';

export interface TemplateOverview {
  uuid: string,
  name: string,
  desc?: string,
  ownedByGroup?: string,
  canEdit: boolean,
  sample?: string
}

export interface NewDocumentResponse {
  documentId: string,
  templateId: string
}

type Sample = {
  name: string,
  uuid: string,
  body: any,
  style: string
}

export const TemplatesList: React.FC = () => {

  const [templates, setTemplates] = useState<Array<TemplateOverview>>([]);
  const [selectedTemplate, setSelectedTemplate] = useState<TemplateOverview | undefined>(undefined);
  const [templateContent, setTemplateContent] = useState<string>('');
  const [samplePreview, setSamplePreview] = useState<Sample | undefined>(undefined);
  const [documentOptions, setDocumentOptions] = useState<DocumentOptions>(new DocumentOptions({ "styles": [], "labels": [] }));
  const [documentStyle, setDocumentStyle] = useState<string>('');
  const [sampleToPreview, setSampleToPreview] = useState<string | undefined>(undefined);

  const [newDocumentName, setNewDocumentName] = useState<string>('whatever');
  const [newTemplateName, setNewTemplateName] = useState<string>('whatever');

  const { push } = useHistory()
  const [showNewDocumentModal, setShowNewDocumentModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showCloneModal, setShowCloneModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showSampleModal, setShowSampleModal] = useState(false);


  useEffect(() => {
    getTemplates()
  }, []);


  const getTemplates = () => {
    fetch('/api/templates')
      .then(response => response.json())
      .then(x => x as Array<TemplateOverview>)
      .then(ts => setTemplates(ts))
      .catch(err => console.log(err))
  }

  const newDocument = (templateID: string, documentName: string) => {
    fetch(`/api/template/${templateID}/new-document`, {
      method: 'POST',
      body: `{"name": "${documentName}"}`
    })
      .then(response => response.json())
      .then(x => x as NewDocumentResponse)
      .then(resp => push(`/edit-document/${resp.documentId}`))
      .catch(err => console.log(err))
  }

  const deleteTemplate = (templateID: string) => {
    fetch(`/api/template/${templateID}`, {
      method: 'DELETE'
    })
      .then(getTemplates)
  }


  const renderDeleteModal = () => {

    const toggle = () => setShowDeleteModal(!showDeleteModal);

    const deleteCurrentTemplate = () => {
      setShowDeleteModal(false);
      deleteTemplate(selectedTemplate!.uuid)
    }

    return (
      <div>
        <Modal isOpen={showDeleteModal} toggle={toggle} >
          <ModalHeader toggle={toggle}>Delete</ModalHeader>
          <ModalBody>
            <div>You are about to permanently delete template&nbsp;
            <span className='dialog-template-name'>
                {selectedTemplate ? selectedTemplate.name : ''}
              </span>
            </div>
            <div>
              Are you sure?
            </div>

          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => deleteCurrentTemplate()}>Delete</Button>
            <Button color="secondary" onClick={toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

  const renderCloneModal = () => {

    const toggle = () => setShowCloneModal(!showCloneModal);

    const cloneTemplate = (template: TemplateOverview, newName: string) => {
      fetch(`/api/template/${template.uuid}/duplicate`, {
        method: 'POST',
        body: `{"name": "${newName}"}`
      })
        .then(getTemplates)
    }

    const cloneCurrentTemplate = () => {
      setShowCloneModal(false);
      cloneTemplate(selectedTemplate!, newTemplateName)
    }

    return (
      <div>
        <Modal isOpen={showCloneModal} toggle={toggle} >
          <ModalHeader toggle={toggle}>Clone</ModalHeader>
          <ModalBody>
            <p>You are about to clone template&nbsp;
            <span className='dialog-template-name'>
                {selectedTemplate ? selectedTemplate.name : ''}
              </span>. You can tweak it and use it to create new CV from it.
            </p>
            <p>
              What will the name of the new template?
            </p>
            <div>
              <Input value={newTemplateName}
                onChange={(e: React.FormEvent<HTMLInputElement>) => { setNewTemplateName(e.currentTarget.value) }} />
            </div>

          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => cloneCurrentTemplate()}>Clone</Button>
            <Button color="secondary" onClick={toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
  const renderEditModal = () => {

    const toggle = () => setShowEditModal(!showEditModal);

    const updateCurrentTemplate = () => {
      setShowCloneModal(false);
      updateTemplate(selectedTemplate!.uuid, templateContent)
    }

    const updateTemplate = (id: string, content: string) => {
      fetch(`/api/template/${id}`, {
        method: 'POST',
        body: `{"content": "${btoa(content)}"}`
      })
    }


    return (
      <div>
        <Modal isOpen={showEditModal} toggle={toggle} className='modal-lg'>
          <ModalHeader toggle={toggle}>Editing <i>{selectedTemplate ? selectedTemplate.name : ''}</i> </ModalHeader>
          <ModalBody>
            <p>
              If you have existing documents based on this template, changes in this templae may affect them.
              Changing the name of a field or a group name will result in an empty value for it.
             </p>

            <CodeEditor name='template-editor'
              content={templateContent}
              lang='json'
              changeHandler={(s: string) => setTemplateContent(s)} />

          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => updateCurrentTemplate()}>Save</Button>
            <Button color="secondary" onClick={toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

  const renderSampleModal = () => {

    const toggle = () => setShowSampleModal(!showSampleModal);

    const node = (
      <div id='previedModal'>
        <Modal isOpen={showSampleModal} toggle={toggle} className='modal-lg'>
          <ModalHeader toggle={toggle}>{samplePreview?.name} </ModalHeader>
          <ModalBody>
            <div className='sample-preview'>
              {selectedTemplate && selectedTemplate.sample ?
                <FormattedDocument uuid={selectedTemplate!.sample!} />
                : <div></div>}
            </div>
          </ModalBody>

          <ModalFooter>
            <Button color="secondary" onClick={toggle}>Close</Button>
            <Button color="primary" onClick={
              () => {
                setShowSampleModal(false)
                setShowNewDocumentModal(true)
              }
            }>Use this Template</Button>
          </ModalFooter>
        </Modal>
      </div>
    );

    return node
  }

  const renderNewDocumentModal = () => {

    const toggleNewDocumentDialog = () => setShowNewDocumentModal(!showNewDocumentModal);
    const createNewDocument = (uuid: string, documentName: string) => {
      setShowNewDocumentModal(false);
      newDocument(uuid, documentName)
    }

    return (
      <div>
        <Modal isOpen={showNewDocumentModal} toggle={toggleNewDocumentDialog} >
          <ModalHeader toggle={toggleNewDocumentDialog}>New Document</ModalHeader>
          <ModalBody>
            <div>You are about to create a new document from template&nbsp;
            <span className='dialog-template-name'>
                {selectedTemplate ? selectedTemplate.name : ''}
              </span>
            </div>
            <div>
              Give it a name:
            </div>
            <div>
              <Input value={newDocumentName}
                onChange={(e: React.FormEvent<HTMLInputElement>) => { setNewDocumentName(e.currentTarget.value) }} />
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => createNewDocument(selectedTemplate!.uuid, newDocumentName)}>Create</Button>
            <Button color="secondary" onClick={toggleNewDocumentDialog}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

  const showNewDocumentDialog = (template: TemplateOverview) => {
    var date = new Date();
    setNewDocumentName(`${template.name} ${date.getFullYear()}-${date.getMonth() + 1}-${date.getDay()} ${randomString(5)}`)
    setSelectedTemplate(template);
    setShowNewDocumentModal(true)
  }

  const showCloneDialog = (template: TemplateOverview) => {
    setNewTemplateName(`${template.name}  copy`)
    setSelectedTemplate(template);
    setShowCloneModal(true)
  }

  const showDeleteDialog = (template: TemplateOverview) => {
    setSelectedTemplate(template);
    setShowDeleteModal(true)
  }

  const showEditDialog = (template: TemplateOverview) => {

    type Template = {
      name: String,
      body: any
    }

    const getTemplate = (id: string) => {
      fetch(`/api/template/${id}`)
        .then(response => response.json())
        .then(x => x as Template)
        .then(x => {
          setTemplateContent(JSON.stringify(x['body'], null, 2));
        })
        .then(x => setShowEditModal(true))
        .catch(err => console.log(err))
    }

    setSelectedTemplate(template);
    getTemplate(template.uuid)

  }

  const showSampleDialog = (template: TemplateOverview) => {

    setSelectedTemplate(template)
    setShowSampleModal(true)
  }

  const downloadPdf = (t: TemplateOverview) => {
    const element = document.createElement("a");
    element.href = `/api/document/${t.sample}/pdf`;
    element.target = 'download'
    document.body.appendChild(element);
    element.click();
  }

  const renderDialogs = () => {
    return (
      <div>
        { renderNewDocumentModal()}
        { renderDeleteModal()}
        { renderCloneModal()}
        { renderEditModal()}
        { renderSampleModal()}
      </div>
    )
  }

  const menu = (t: TemplateOverview) => [
    [new MenuItem(icon.iconSparkle, 'Create', showNewDocumentDialog)],
    [
      new MenuItem(icon.iconSparkle, 'Clone', showCloneDialog),
      new MenuItem(icon.pencilAlt, 'Edit', t.canEdit ? showEditDialog : undefined),
      new MenuItem(icon.pencilAlt, 'PDF', downloadPdf)
    ],
    [
      new MenuItem(icon.iconTrash, 'Remove', t.canEdit ? showDeleteDialog : undefined)

    ]
  ]
  return (
    <div style={{ width: '100%' }}>
      {renderDialogs()}
      <div className="flex flex-col" >
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead>
                  <tr>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Template
                    </th>
                    <th className="px-6 py-3 bg-gray-50"></th>
                    <th className="px-6 py-3 bg-gray-50"></th>
                    <th className="px-6 py-3 bg-gray-50"></th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {templates.map((template, ix) => (
                    <tr key={ix}>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex items-center">

                          <div className="ml-4">
                            <div className="text-sm leading-5 font-medium text-gray-900">
                              {template.name}
                            </div>
                            <div className="text-sm leading-5 text-gray-500">
                              {template.desc}
                            </div>
                          </div>
                        </div>
                      </td>

                      <td className="px-1 py-4 whitespace-nowrap">
                        <div style={{ width: '2em' }}
                          onClick={() => showSampleDialog(template)}
                        >
                          <icon.eye />
                        </div>
                      </td>

                      <td className="px-1 py-4 whitespace-nowrap">
                        <span className="inline-flex rounded-md shadow-sm">
                          <button type="button" 
                          className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-indigo-700 bg-indigo-100 hover:bg-indigo-50 focus:outline-none focus:border-indigo-300 focus:shadow-outline-indigo active:bg-indigo-200 transition ease-in-out duration-150"
                          onClick={() => showNewDocumentDialog(template)}>
                            Create from this template
                            <icon.pencilAlt/>
                          </button>
                        </span>
                      </td>


                      <td className="px-6 py-4 whitespace-nowrap text-right text-sm leading-5 font-medium">
                        <ItemMenu menuBlocks={menu(template)} t={template} />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  )

}

/* <RenderControl
  prefix='new-document'
  tooltipeText='Create New Document'
  element={template}
  icon={faPlusSquare}
  action={showNewDocumentDialog} />
<RenderControl
  cond={t => t.canEdit}
  prefix='edit-template'
  tooltipeText='Edit Template'
  element={template}
  icon={faUserEdit}
  action={showEditDialog} />
<RenderControl
  prefix='clone-template'
  tooltipeText='Clone Template'
  element={template}
  icon={faCopy}
  action={showCloneDialog} />
<RenderControl
  cond={t => t.canEdit}
  prefix='delete-template'
  tooltipeText='Delete Template'
  element={template}
  icon={faTrashAlt}
  action={showDeleteDialog} />
<RenderControl
  cond={t => !!t.sample}
  prefix='show-sample'
  tooltipeText='View an Example'
  element={template}
  icon={faEye}
  action={showSampleDialog} />
<RenderControl
  cond={t => !!t.sample}
  prefix='download-pdf'
  tooltipeText='Download example as PDF'
  element={template}
  icon={faFilePdf}
  action={downloadPdf} /> */





