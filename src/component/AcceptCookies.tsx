import React from 'react';
import { AppUser } from '../App'
import './AcceptCookies.css';

import { Button } from 'reactstrap';

type AcceptCookiesProps = {
  user: AppUser | undefined, 
  updateUser: ((url: RequestInfo) => void)
}



export const AcceptCookies = ({ user,  updateUser }: AcceptCookiesProps) => {

  const accept = () => {
    updateUser('/api/user/accept-cookies')
  }

  if (user && !user.acceptedCookies)
    return (<div className="accept-cookies">
      <span className="accept-cookies-left">We Use cookies</span>
      <Button variant="success" size="lg" onClick={accept}>Accept</Button></div>)
  else return <div></div>
}