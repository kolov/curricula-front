import React, { useState } from 'react';
import { AppUser } from '../App'
import { CognitoClientId, CognitoUrl } from '../Utils';
import LoginCognito from './LoginCognito';


type CurrentUserProps = {
  user: AppUser | undefined,
  updateUser: ((url: RequestInfo) => void)
}

export const CurrentUser = ({ user, updateUser }: CurrentUserProps) => {

  const [open, setOpen] = useState<boolean>(false)

  const toggleOpen = () => setOpen(!open)

  const logout = () => {
    fetch('/api/logout')
      .then(() => updateUser('/api/user/info'))
      .catch(err => console.log(err))
  }


  const signout = () => {
    window.location.href = `${CognitoUrl}/logout?client_id=${CognitoClientId}&logout_uri=https://${window.location.hostname}/cognito/logout`
  }


  return (!user || !user.authenticated) ? (<div>
    <LoginCognito />
  </div>)
    :
    <div className="ml-3 relative">
      <div>
        <button className="max-w-xs flex items-center text-sm rounded-full text-white focus:outline-none focus:shadow-solid" id="user-menu" aria-label="User menu" aria-haspopup="true" onClick={toggleOpen}>
          <img className="h-8 w-8 rounded-full" src={user.picture} alt="" />
        </button>
      </div>
      <div className="space-y-1">
        <div className="text-base font-medium leading-none text-white">{user.name}</div>
      </div>

      {open ?
        (<div className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg">
          <div className="py-1 rounded-md bg-white shadow-xs">
            <a href="#" className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" onClick={signout}>Sign out</a>
          </div>
        </div>) : <div></div>}
    </div>

}