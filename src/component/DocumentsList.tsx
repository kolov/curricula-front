import React, { useState, useEffect } from 'react';

import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row, Tooltip } from 'reactstrap';
import { useHistory } from 'react-router'
import { faEye, faFilePdf, faPenAlt, faTint, faTrash, faTrashAlt, faUserEdit, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { RenderControl } from '../model/Common';
import { FormattedDocument } from './FormattedDocument';
import * as icon from './Heroicons';
import { ItemMenu, MenuItem } from './TemplateItemMenu1';

export interface DocumentOverview {
  uuid: string,
  name: string,
  templateUuid: string,
  canEdit: boolean
}




export const DocumentsList: React.FC = () => {

  const { push } = useHistory()

  const [docs, setDocs] = useState<Array<DocumentOverview>>(Array(0));
  const [showRenameDocumentModal, setShowRenameDocumentModal] = useState(false);
  const [selectedDocument, setSelectedDocument] = useState<DocumentOverview | undefined>(undefined)
  const [newDocumentName, setNewDocumentName] = useState('')
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showPreviewModal, setShowPreviewModal] = useState(false);

  useEffect(() => {
    getDocs()
  }, []);

  const getDocs = () => {
    fetch('/api/documents')
      .then(response => response.json())
      .then(x => x as Array<DocumentOverview>)
      .then(ts => setDocs(ts))
      .catch(err => console.log(err))
  }

  const deleteDocument = (uuid: string) => {
    fetch(`/api/document/${uuid}`, {
      method: 'DELETE'
    })
      .then(getDocs)
  }

  const renderPreviewModal = () => {

    const toggle = () => setShowPreviewModal(!showPreviewModal);

    return (
      <div>
        <Modal isOpen={showPreviewModal} toggle={toggle} className='modal-lg'>
          <ModalHeader toggle={toggle}>{selectedDocument?.name} </ModalHeader>
          <ModalBody>
            <div className='sample-preview'>
              {selectedDocument ?
                <FormattedDocument uuid={selectedDocument.uuid} />
                : <div></div>}
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={toggle}>Close</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

  const renderDeleteModal = () => {

    const toggle = () => setShowDeleteModal(!showDeleteModal);

    const deleteCurrentDocument = (uuid: string) => {
      setShowDeleteModal(false);
      deleteDocument(uuid)
    }

    return (
      <div>
        <Modal isOpen={showDeleteModal} toggle={toggle} >
          <ModalHeader toggle={toggle}>Delete</ModalHeader>
          <ModalBody>
            <div>You are about to permanently delete document&nbsp;
            <span className='dialog-template-name'>
                {selectedDocument ? selectedDocument.name : ''}
              </span>
            </div>
            <div>
              Are you sure?
            </div>

          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => deleteCurrentDocument(selectedDocument!.uuid)}>Delete</Button>
            <Button color="secondary" onClick={toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

  const renderRenameDocumentModal = () => {

    const toggle = () => setShowRenameDocumentModal(!showRenameDocumentModal);
    const renameDocument = (uuid: string, documentName: string) => {
      setShowRenameDocumentModal(false);
      fetch(`/api/document/${uuid}`, {
        method: 'POST',
        body: `{"name": "${documentName}"}`
      })
        .then(getDocs)
    }

    return (
      <div>
        <Modal isOpen={showRenameDocumentModal} toggle={toggle} >
          <ModalHeader toggle={toggle}>Rename Document</ModalHeader>
          <ModalBody>
            <div>Please enter a new name
            </div>
            <div>
              <Input value={newDocumentName}
                onChange={(e: React.FormEvent<HTMLInputElement>) => {
                  console.log(e)
                  setNewDocumentName(e.currentTarget.value)
                }} />
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" outline={true} size="sm" disabled={selectedDocument?.name == newDocumentName}
              onClick={() => renameDocument(selectedDocument!.uuid, newDocumentName)}>
              Rename
            </Button>
            <Button color="secondary" onClick={toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

  const openRenameDialog = (doc: DocumentOverview) => {
    setSelectedDocument(doc)
    setNewDocumentName(doc.name)
    setShowRenameDocumentModal(true)
  }

  const showDeleteDialog = (d: DocumentOverview) => {
    setSelectedDocument(d);
    setShowDeleteModal(true)
  }

  const showPreviewDialog = (d: DocumentOverview) => {
    setSelectedDocument(d);
    setShowPreviewModal(true)
  }

  const downloadFile = (doc: DocumentOverview) => {
    const element = document.createElement("a");

    element.href = `/api/document/${doc.uuid}/pdf`;
    element.target = 'download'
    document.body.appendChild(element);
    element.click();
  }

  const editDocument = (d: DocumentOverview) => push(`edit-document/${d.uuid}`)

  const menu = [
    [new MenuItem<DocumentOverview>(icon.pencilAlt, 'Edit', editDocument),
    new MenuItem<DocumentOverview>(icon.iconSparkle, 'Rename', openRenameDialog)],
    [
      new MenuItem<DocumentOverview>(icon.iconSparkle, 'DownloadPDF', downloadFile)
    ],
    [
      new MenuItem<DocumentOverview>(icon.iconTrash, 'Delete', showDeleteDialog)
    ]
  ]

  return (

    (docs.length > 0) ?

      <div>
        <p>Below are your documents. You can create a new document
        from one of the templates above.&nbsp;
                    <a href="/about">Learn more</a>
        </p>


        <div className="flex flex-col" >
          <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
              <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table className="min-w-full divide-y divide-gray-200">
                  <thead>
                    <tr>
                      <th className="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                        Document
                    </th>
                      <th className="px-6 py-3 bg-gray-50"></th>
                      <th className="px-6 py-3 bg-gray-50"></th>
                      <th className="px-6 py-3 bg-gray-50"></th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {docs.map((doc, ix) => (
                      <tr key={ix}>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <div className="flex items-center">

                            <div className="ml-4">
                              <div className="text-sm leading-5 font-medium text-gray-900">
                                {doc.name}
                              </div>
                              <div className="text-sm leading-5 text-gray-500">
                                Desc
                            </div>
                            </div>
                          </div>
                        </td>

                        <td className="px-1 py-4 whitespace-nowrap">
                          <div style={{ width: '2em' }} onClick={() => showPreviewDialog(doc)}
                          >
                            <icon.eye />

                          </div>
                        </td>

                        <td className="px-1 py-4 whitespace-nowrap">
                        <span className="inline-flex rounded-md shadow-sm">
                          <button type="button" 
                          className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-indigo-700 bg-indigo-100 hover:bg-indigo-50 focus:outline-none focus:border-indigo-300 focus:shadow-outline-indigo active:bg-indigo-200 transition ease-in-out duration-150"
                          onClick={() => editDocument(doc)}>
                            Edit
                            <icon.pencilAlt/>
                          </button>
                        </span>
                      </td>


                        <td className="px-6 py-4 whitespace-nowrap text-right text-sm leading-5 font-medium">
                          <ItemMenu<DocumentOverview> menuBlocks={menu} t={doc} />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div>{renderRenameDocumentModal()}</div>
        <div>{renderDeleteModal()}</div>
        <div>{renderPreviewModal()}</div>
      </div>
      :
      <div className="bg-gray-50">
        <div className="max-w-screen-xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between">
          <h2 className="text-3xl leading-9 font-extrabold tracking-tight text-gray-900 sm:text-4xl sm:leading-10">
           You have no CVs yet
      <br />
            <span className="text-indigo-600">Start creating your first CV</span>
          </h2>
          <div className="mt-8 flex lg:flex-shrink-0 lg:mt-0">
            <div className="inline-flex rounded-md shadow">
              <a href="/templates" className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline transition duration-150 ease-in-out">
                Go to the Templates
        </a>
            </div>
            <div className="ml-3 inline-flex rounded-md shadow">
              <a href="/about" className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-indigo-600 bg-white hover:text-indigo-500 focus:outline-none focus:shadow-outline transition duration-150 ease-in-out">
                Learn more
        </a>
            </div>
          </div>
        </div>
      </div>
  );

}