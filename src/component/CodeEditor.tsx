import React, { useState } from 'react';
import Editor from 'react-simple-code-editor';
import * as Prism from 'prismjs';


import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';
import 'prismjs/themes/prism.css';


type CodeEditorProps = {
  name: string,
  content: string,
  lang: string,
  changeHandler: (value: string) => void 
}



export const CodeEditor = ({ name, content, lang, changeHandler}: CodeEditorProps) => {

  const [editedContent, setEditedContent] = useState<string>(content);
  
  return (<Editor
    preClassName={`${name}-pre` }
    textareaClassName={`${name}-textarea`}
    value={editedContent}
    onValueChange={s => {setEditedContent(s); changeHandler(s)}}
    highlight={code => Prism.highlight(code, Prism.languages.css, lang)}
    tabSize={2}
    padding={10} 
    style={{
      fontFamily: '"Fira code", "Fira Mono", monospace',
      fontSize: 12,
      overflow: 'scroll'
    }}
  />)
}