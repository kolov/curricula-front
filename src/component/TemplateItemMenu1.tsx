import React, { CSSProperties, useState, MouseEvent, Component } from 'react';
import { AppUser } from '../App'
import './AcceptCookies.css';

import { Button } from 'reactstrap';
import * as icon from './Heroicons'; 

export class MenuItem<T> {
  icon: () => JSX.Element;
  text: string
  action?: (t: T) => void

  constructor(icon: () => JSX.Element,
    text: string,
    action?: (t: T) => void) {
    this.icon = icon
    this.text = text
    this.action = action
  }
}

type ItemMenuProps<T> = {
  menuBlocks: Array<Array<MenuItem<T>>>,
  t: T
}

export function ItemMenu<T>({ menuBlocks, t }: ItemMenuProps<T>) {

  const [show, setShow] = useState<boolean>(false)
  const [menuStyle, setMenuStyle] = useState<CSSProperties>({})
  const toggle = (clickEvent: MouseEvent) => {
    setMenuStyle({
      left: `${clickEvent.clientX}px`,
      right: `${clickEvent.clientX}px`,
      position: 'fixed',
      zIndex: 100
    })
    setShow(!show)
  }

  function closeAndAction(template: T, action?: ((t: T) => void)) {
    if (action) {
      setShow(false);
      action(template)
    }
  }

  const menuItemClass = (enabled: boolean) => {
    return (enabled ?
      "group flex items-center px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
      :
      "group flex items-center px-4 py-2 text-sm leading-5 text-gray-200 hover:bg-gray-100 hover:text-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-200"
    )
  }
  {/* <!--
    Dropdown panel, show/hide based on dropdown state.

    Entering: "transition ease-out duration-100"
      From: "transform opacity-0 scale-95"
      To: "transform opacity-100 scale-100"
    Leaving: "transition ease-in duration-75"
      From: "transform opacity-100 scale-100"
      To: "transform opacity-0 scale-95"
  --> */}
  return (

    <div>
      <div className="relative inline-block text-left">

        <button className="flex items-center text-gray-400 hover:text-gray-600 
        focus:outline-none focus:text-gray-600" aria-label="Options" id="options-menu"
          aria-haspopup="true" aria-expanded="true"
          onClick={e => toggle(e)}>
          <icon.sandwich />
        </button>
      </div>

      {show ?
        <div style={menuStyle}>

          <div className="origin-toƒp-right absolute right-0 mt-2 w-56 rounded-md shadow-lg" >
            <div className="rounded-md bg-white shadow-xs" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">

              {menuBlocks.map((block, ixb) => (
                <div >
                  <div className="py-1">
                    {block.map((item, iix) =>
                      <a href="#" key={`${ixb}_${iix}`}
                        className={menuItemClass(item.action != undefined)}
                        role="menuitem" onClick={() => closeAndAction(t, item.action)}>
                        {item.icon()}{item.text}</a>
                    )}
                  </div>
                  <div className="border-t border-gray-100"></div>
                </div>
              ))}

            </div>
          </div>
        </div>
        : <div></div>
      }
    </div >

  )
}