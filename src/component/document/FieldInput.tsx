import React, { useState, ChangeEvent } from 'react';
import { Input, Label, Tooltip } from 'reactstrap';
import { InputValue, FieldEntry, TextInputField } from '../../model/DocumentModel'
import { idFromPath } from '../../Utils';
import { DocumentIdContext } from './GroupInput';


export type FieldInputComponentProps = {
    fieldEntry: FieldEntry
}


export function FieldInputComponent(props: FieldInputComponentProps) {


    return (
        <div className="cui-field-input form-group">
            <Label for={idFromPath(props.fieldEntry.path)}>{props.fieldEntry.translatedLabel}</Label>
            { props.fieldEntry.valueInputField.renderComponent(props.fieldEntry.path)}
            {props.fieldEntry.desc ?
                <small className="form-text text-muted">{props.fieldEntry.desc}</small>
                : <div></div>}
        </div>
    )

}

export type TextInputComponentProps = {
    field: TextInputField
}



export function TextInputComponent(props: TextInputComponentProps) {
    const docid = React.useContext(DocumentIdContext)!

    const [firstValue, setFirstValue] = useState<string | undefined>(props.field.inputValue.value || '')
    const [value, setValue] = useState<string | undefined>(props.field.inputValue.value || '')


    const updateValue = (e: ChangeEvent<HTMLInputElement>) => {
        if (firstValue !== value) {
            fetch(`/api/document/${docid}/edit`, {
                method: 'POST',
                body: new URLSearchParams({
                    'command': 'update',
                    'path': props.field.path,
                    'value': value ? value : ""
                })
            })
                .then(resp => console.log(resp))
                .catch(err => console.log(err))
        }
    }

    const classNames = `form-control cui-text-input cui-text-input-${props.field._label}`
        + (props.field.inline ? 'cui-text-inline' : '')

    return (<div
        className={`cui-around-text-input cui-around-text-input-${props.field._label}`}>
        <Input
            id={idFromPath(props.field.path)}
            type={props.field.multiline ? 'textarea' : 'text'}
            rows={props.field.lines ? props.field.lines : ''}
            bsSize='sm'
            value={value}
            width={props.field.inputLength ?  `${props.field.inputLength}em`: 'auto'}
            className={classNames}
            onChange={e => { setValue(e.target.value); }}
            onBlur={e => updateValue(e)}
            onFocus={e => { setFirstValue(e.target.value); setValue(e.target.value) }}>
        </Input>
    </div>
    )
}



export type DateInputComponentProps = {
    name: string,
    path: string,
    fieldValue: InputValue
}


export class DateInputComponent extends React.Component<DateInputComponentProps, {}>{

    render() {
        return (<div className="cui-date-input">
            {/* <div className="field-input">  {this.props.name}</div> */}
            <div><input type='text' value={this.props.fieldValue.value} ></input>
            </div>
        </div>
        )
    }

}

