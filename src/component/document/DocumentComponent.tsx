import React from 'react';  
import { InputElement } from '../../model/DocumentModel'
import { Row, Col, Card, CardBody } from 'reactstrap';


export type DocumentComponentProps = {
    name: string,
    body: Array<InputElement>
}


export class DocumentComponent extends React.Component<DocumentComponentProps, {}>{

    render() {
        return (
            <Row> 
                <Col xs="12">
                    <div className="cu-document">                    
                        <Card>
                            <CardBody>
                            {
                                this.props.body.map((el, ixElement) => {
                                    return (<div key={ixElement}>
                                        {el.renderComponent('/' + el.label())}
                                    </div>)
                                }
                                )
                            }
                            </CardBody>
                        </Card>
                    </div>
                </Col>
            </Row>
        )
    }
}