import React, { useState, useEffect, createContext } from 'react'; // let's also import Component
import { GroupEntry, InputElement, Multiplicity } from '../../model/DocumentModel'
import { Card, CardBody, CardTitle, Collapse, Tooltip } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronCircleDown, faChevronCircleUp, faMinusSquare, faPlusSquare, faUserEdit } from '@fortawesome/free-solid-svg-icons';

export type GroupInputComponentProps = {
    name: string,
    translatedName: string,
    translatedInstanceName: string,
    path: string,
    multiplicity: Multiplicity,
    body: Array<Array<InputElement>>
}

export const DocumentIdContext = createContext<string | undefined>(undefined)
export const DocumentRefreshContext = createContext<(() => void) | undefined>(undefined)


export function GroupInputComponent(props: GroupInputComponentProps) {

    const docid = React.useContext(DocumentIdContext)!
    const refreshDocument: () => void = React.useContext(DocumentRefreshContext)!

    const [children, setChildren] = useState<Array<Array<InputElement>> | undefined>(undefined);

    const canInsertAt = () => {
        return (props.multiplicity.maxOccurs === undefined) || (props.multiplicity.maxOccurs! > props.body.length)
    }

    const canDeleteAt = () => {
        return (props.multiplicity.minOccurs < props.body.length)
    }

    const updateGroup = (cmd: string, path: string, ix: number) => {

        fetch(`/api/document/${docid}/edit`, {
            method: 'POST',
            body: new URLSearchParams({
                'command': cmd,
                'path': path,
                'ix': ix.toString()
            })
        })
            .then(resp => console.log(resp))
            .then(x => {
                console.log('About to refresh')
                setChildren([])
                refreshDocument()

            })
            .catch(err => console.log(err))

    }

    const insertGroup = (ix: number) => {
        updateGroup('insert', `${props.path}`, ix)
    }

    const deleteGroup = (ix: number) => {
        updateGroup('delete', `${props.path}`, ix)
    }

    const canHaveMoreChildren = () => {
        return !props.multiplicity.maxOccurs || props.multiplicity.maxOccurs > 1
    }

    const renderChildren = (groupInstances: Array<Array<InputElement>>) => {

        return groupInstances.map((groupInstance, ixGroup) => {
            const prefix = props.path + '_' + ixGroup;
            const collapsable = canHaveMoreChildren() && groupInstance.length > 1

            return <div key={prefix} className={`cui-group-instance cui-group-instance-${props.name} cui-group-instance-no-${ixGroup}`}>
                <MyCard name={props.name}
                    translatedName={props.translatedInstanceName}
                    collapsable={collapsable}
                    showTitle={collapsable}
                    cardType='instance'
                    body={
                        <div>
                            <div className="cui-group-instance-elements">
                                {
                                    // elements of the group
                                    groupInstance.map((el, ixElement) => {
                                        return (<div key={ixElement}>
                                            <div >
                                                {el.renderComponent(prefix + '_' + ixElement)}
                                            </div>
                                        </div>
                                        )
                                    }
                                    )
                                }
                            </div>
                            <div className="cui-group-instance-multiplicity-controls">
                                {props.multiplicity.maxOccurs == 1 ? (<div></div>) :
                                    (<div>
                                        <RenderInsertBeforeControl ix={ixGroup} path={props.path} disabled={!canInsertAt()} />
                                        <RenderDeleteControl ix={ixGroup} path={props.path} disabled={!canDeleteAt()} />
                                        <RenderInsertAfterControl ix={ixGroup} path={props.path} disabled={!canInsertAt()} />
                                    </div>)}
                            </div>
                        </div>
                    }
                ></MyCard>
            </div>
        })
    }



    type InsertProps = {
        ix: number;
        path: string;
        disabled: boolean;
    }
    const RenderInsertBeforeControl = ({ ix, path, disabled }: InsertProps) => {
        const p = path.replace(/\//g, '').replace(/\[/g, '').replace(/\]/g, '')
        const color = disabled ? 'gray' : 'black'
        const [tooltipOpen, settooltipOpen] = useState<boolean>(false)
        return (
            <div className='insert-before-instance-button'>
                <FontAwesomeIcon icon={faPlusSquare} color={color} onClick={() => insertGroup(ix)} id={`insert-before-${p}-${ix}`} />
                <Tooltip placement="right" isOpen={tooltipOpen} target={`insert-before-${p}-${ix}`} toggle={e => disabled ? {} : settooltipOpen(!tooltipOpen)}>
                    Insert Before
            </Tooltip></div>
        )
    }
    const RenderInsertAfterControl = ({ ix, path, disabled }: InsertProps) => {
        const p = path.replace(/\//g, '').replace(/\[/g, '').replace(/\]/g, '')
        const [tooltipOpen, settooltipOpen] = useState<boolean>(false)
        const color = disabled ? 'gray' : 'black'
        return (
            <div className='insert-after-instance-button'>
                <FontAwesomeIcon icon={faPlusSquare} color={color} onClick={() => insertGroup(ix + 1)} id={`insert-after-${p}-${ix}`} />
                <Tooltip placement="right" isOpen={tooltipOpen} target={`insert-after-${p}-${ix}`} toggle={e => disabled ? {} : settooltipOpen(!tooltipOpen)}>
                    Insert After
            </Tooltip></div>
        )
    }
    const RenderDeleteControl = ({ ix, path, disabled }: InsertProps) => {
        const p = path.replace(/\//g, '').replace(/\[/g, '').replace(/\]/g, '')
        const [tooltipOpen, settooltipOpen] = useState<boolean>(false)
        const color = disabled ? 'gray' : 'black'
        return (
            <div className='delete-instance-button'>
                <FontAwesomeIcon icon={faMinusSquare} color={color} onClick={() => deleteGroup(ix)} id={`delete-after-${p}-${ix}`} />
                <Tooltip placement="right" isOpen={tooltipOpen} target={`delete-after-${p}-${ix}`} toggle={e => disabled ? {} : settooltipOpen(!tooltipOpen)}>
                    Delete
            </Tooltip></div>
        )
    }

    useEffect(() => {
        setChildren(props.body)
    }, [props.body]);

    return <MyCard name={props.name}
        translatedName={props.translatedName}
        collapsable={children && children.length > 0 && children[0].length > 0 &&
            (children[0][0] instanceof GroupEntry || children[0].length > 1)}
        cardType='entry'
        showTitle={true}
        body={
            (children) ?
                renderChildren(children)
                :
                (<div>Waiting...</div>)
        }
    ></MyCard>
}

type MyCardProps = {
    name: string,
    translatedName: string,
    collapsable?: boolean,
    showTitle?: boolean,
    cardType: string,
    body: any
}



export const MyCard = ({ name, translatedName, collapsable, showTitle, cardType, body }: MyCardProps) => {

    const [isOpen, setIsOpen] = useState(true);
    const toggle = () => setIsOpen(!isOpen);


    return (
        <div className={`cui-group-entry cui-group-entry-${name}`}>
            <Card>
                {showTitle ?
                    (<CardBody>
                        <CardTitle>
                            <div className={`cui-group-${cardType}-label cui-group-${cardType}-label-${name}`}>{translatedName}</div>
                            {/* Collapse arrow - when the group *instance* has 1 child*/}
                            {collapsable ?
                                (<div className='group-collapse-button' onClick={toggle} >
                                    <FontAwesomeIcon icon={isOpen ? faChevronCircleUp : faChevronCircleDown} />
                                </div>)
                                : (<div></div>)}
                            <div className={`cui-group-${cardType}-multiplicity-controls`}>
                            </div>
                        </CardTitle>
                    </CardBody>) : <div></div>}
                <CardBody>
                    <Collapse isOpen={isOpen}>
                        <div> {
                            body
                        }
                        </div>
                    </Collapse>
                </CardBody>
            </Card>


        </div>)
}