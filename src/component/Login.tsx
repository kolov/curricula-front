import { faGoogle } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Button } from 'reactstrap';

type LoginProps = {
  name: string
}

const Login = ({ name }: LoginProps) => {
  return <Button outline={true} color="primary" href={"/api/login/" + name} >
    Login <FontAwesomeIcon icon={faGoogle} />
  </Button>
}



export default Login;
