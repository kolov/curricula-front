import React from 'react';
import { DocumentComponent } from '../component/document/DocumentComponent';
import { GroupInputComponent } from '../component/document/GroupInput';
import { FieldInputComponent, TextInputComponent, DateInputComponent } from '../component/document/FieldInput';


export interface InputComponent {
    renderComponent(path: string): React.ReactElement;
    label(): string;
}

export interface ValueInputField extends InputComponent {
}


export class InputValue {
    value?: string;
    constructor(json: any) {
        if (json['value'])
            this.value = json['value']
    }
}

export class TextInputField implements ValueInputField {
    path: string
    _label: string
    translatedLabel: string
    maxLength?: number
    inputLength?: number
    pattern?: string
    inputValue: InputValue
    desc?: string 
    multiline?: boolean
    lines?: number
    inline?: boolean
    constructor(path: string, json: any) {
        this._label = json['label']
        this.translatedLabel = json['translatedLabel']
        this.path = path + '/' + this._label
        this.maxLength = json['input']['maxLength']
        this.inputLength = json['input']['inputLength']
        this.lines = json['input']['lines']
        this.pattern = json['pattern']
        this.multiline = json['input']['multiline']  
        this.inline = json['input']['inline']  
        this.inputValue = new InputValue(json['value'])
        this.desc = json['desc']
    }

    label(): string { return this._label }

    renderComponent(): React.ReactElement {
        return React.createElement(TextInputComponent,
            {
                field: this
            }, null)
    }
}


export class DateInputField implements ValueInputField {
    path: string;
    _label: string;
    inputValue: InputValue;
    desc: string | undefined
    constructor(path: string, label: string, json: any, jsonValue: any, desc: string | undefined) {
        this.path = path
        this._label = label
        this.inputValue = new InputValue(jsonValue)
        this.desc = desc
    }

    label(): string { return this._label }

    renderComponent(): React.ReactElement {
        return React.createElement(DateInputComponent,
            {
                name: this._label,
                path: this.path,
                fieldValue: this.inputValue
            }, null)
    }
}

export class Multiplicity {
    minOccurs: number;
    maxOccurs?: number;
    constructor(json: any) {
        this.minOccurs = json['minOccurs']
        this.maxOccurs = json['maxOccurs']
    }
}

export interface InputElement extends InputComponent {

}

export class FieldEntry implements InputElement {
    path: string;
    desc?: string;
    _label: string;
    translatedLabel: string;
    valueInputField: ValueInputField;
    constructor(path: string, json: any) {
        this._label = json['label']
        this.translatedLabel = json['translatedLabel']
        this.desc = json['desc']
        this.path = path + '/' + this._label
        this.valueInputField = parseValueInput(path, json)
    }

    label(): string { return this._label }

    renderComponent(): React.ReactElement {
        return React.createElement(FieldInputComponent,
            {
                fieldEntry: this
            }, null)
    }
}



export class GroupEntry implements InputElement {
    path: string;
    desc?: string;
    _label: string;
    translatedLabel: string;
    translatedInstanceLabel: string;
    multiplicity: Multiplicity;
    instances: Array<Array<InputElement>>;
    constructor(path: string, json: any) {
        this._label = json['label']
        this.translatedLabel = json['translatedLabel']
        this.translatedInstanceLabel = json['translatedInstanceLabel']
        this.desc = json['desc']
        this.path = path + '/' + this._label;
        this.multiplicity = new Multiplicity(json['multiplicity'])
        this.instances = []
        const content = json['groupInstances']
        content.forEach((el: any, ix: number) => {
            this.instances.push(parseArrayOfElements(this.path + '[' + ix + ']', el))
        })

        console.log(`Number of ${this._label} instances ` + this.instances.length)
    }

    label(): string { return this._label }

    renderComponent(): React.ReactElement {
        return React.createElement(GroupInputComponent,
            {
                name: this._label,
                translatedName: this.translatedLabel,
                translatedInstanceName: this.translatedInstanceLabel,
                path: this.path,
                multiplicity: this.multiplicity,
                body: this.instances
            }, null)
    }
}



export class Document {
    uuid: string;
    name: string;
    templateUuid: string;
    inputElements: Array<InputElement>;


    constructor(json: any) {
        this.uuid = json['uuid']
        this.templateUuid = json['templateUuid']
        this.name = json['name']
        this.inputElements = parseArrayOfElements('', json['body'])
    }

    renderForEdit() {
        console.log('Called Document.renderForEdit')
        return (<div>
            {React.createElement(DocumentComponent, { name: this.name, body: this.inputElements }, null)}
        </div>)
    }
}

export function parseArrayOfElements(path: string, json: any): Array<InputElement> {
    var result = Array(0);
    json.forEach((el: any, ix: number) => {
        result.push(parseInput(path, el))
    });
    return result;
}

export function parseInput(path: string, json: any): InputElement {
    if (json['GroupEntry']) {
        return new GroupEntry(path, json['GroupEntry'])
    } else if (json['FieldEntry']) {
        return new FieldEntry(path, json['FieldEntry'])
    } else
        throw new TypeError("Invaid json1: " + json.toString())
}

function parseValueInput(path: string, json: any): ValueInputField {
    if (json['input']['type'] == 'text') {
        return new TextInputField(path, json)
    } else if (json['input']['Date']) {
        return new DateInputField(path, json['label'], json['input']['Date'], json['value'], json['desc'])
    } else
        throw new TypeError("Invaid jsonDesc: " + json['input'].toString())
}
