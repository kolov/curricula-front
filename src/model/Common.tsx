import { IconDefinition } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React, { useState } from "react"
import { Button, Tooltip } from "reactstrap"

export interface HasUuid {
    uuid: string;
}

type RenderControlProps<T extends HasUuid> = {
    cond?: (t: T) => boolean,
    prefix: string,
    tooltipeText: string,
    element: T
    icon: IconDefinition,
    action: (t: T) => void
}

export function RenderControl<T extends HasUuid>({ cond, prefix, tooltipeText, element, icon, action }: RenderControlProps<T>) {
    const [tooltipOpen, settooltipOpen] = useState<boolean>(false)
    return (
        <div className='element-control' id={`${prefix}-${element.uuid}`} >
            <Button outline={true} color="primary" disabled={cond && !cond(element)} onClick={() => action(element)}  >
                <FontAwesomeIcon icon={icon}  />
            </Button>
            <Tooltip placement="top" isOpen={tooltipOpen} target={`${prefix}-${element.uuid}`} toggle={e => settooltipOpen(!tooltipOpen)}>
                {tooltipeText}
            </Tooltip>
        </div>
    )
}